import {defineStore} from 'pinia'

export const useQuarterStore = defineStore('quarter', {
    state: () => {
        return {quarterValue: 0}
    },
})